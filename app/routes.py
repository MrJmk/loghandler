from app import app
from flask import jsonify
import os

@app.route('/api/listlog/index')  
def index():
    dictionnaire = os.listdir('LogFileTest')
    listLog = []
    for element in dictionnaire:
        pathLog = 'LogFileTest\\'+element
        with open(pathLog, 'r') as fichier:
            line = fichier.readline()
            while line:
                logLine = {}
                logLine["server"] = "127.0.0.1- LAPTOP-1319F49T"
                logLine["file"] = element
                logLine["date"] = element[:24]

                partLine = line.split(' ', 2)
                logLine["position"] = partLine[0]
                logLine["level"] = partLine[1]
                logLine["message"] = partLine[2]
                listLog.append(logLine)
                line = fichier.readline()

    return jsonify(listLog)